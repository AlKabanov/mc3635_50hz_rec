

#define RF_DIO0_PORT gpioPortC
#define RF_DIO0_PIN 14
#define RF_DIO0_MASK (1<<RF_DIO0_PIN)
/*******************************************************************************
 *************************   TYPEDEFS   ****************************************
 ******************************************************************************/

/** @addtogroup GPIO_TYPEDEFS Typedefs
 * @{ */

/** Interrupt process callback function */
typedef void (*GPIO_callback_t)(void);

/** GPIO IRQ hook structure */
typedef struct {
	GPIO_callback_t callback;	///< callback function to handle this GPIO IRQ
	uint32_t mask;					///< mask to apply for matching this GPIO IRQ
} GPIO_hook_t;

typedef enum{
  rfDIO0 = 0,
  button = 1,
  chew = 2,
 // rumination = 3,
  hookSize,
}gpioInt_t;

/** @} */

/***************************************************************************//**
 * @brief
 *   Register a callback function for processing interrupts matching a given bit mask.
 *
 * @ param[in] type
 *   The type of callback to set up.
 *
 * @param[in] callback
 *   Pointer to callback function called when an interrupt matching the mask is
 *   received.
 *
 * @param[in] mask
 *   Mask for testing a received even interrupts against.
 ******************************************************************************/
static __INLINE void GPIO_SetCallback(gpioInt_t type, GPIO_callback_t callback, uint32_t mask)
{
	extern GPIO_hook_t GPIO_Hooks[];

	GPIO_Hooks[type].callback = callback;
	GPIO_Hooks[type].mask = mask;
}





void gpioSetup(void);
void gpioSetLED(void);
void gpioClearLED(void);
void gpioToggleLED(void);

void gpioLDOOn(void);
void gpioLDOOff(void);
void gpioTEMPPWRON(void);
void gpioTEMPPWROFF(void);
void gpioPULSEPWRON(void);
void gpioPULSEPWROFF(void);

// set radio RST pin to given value (or keep floating!)
void gpioRST(uint8_t val);

// val ==1  => tx 1, rx 0 ; val == 0 => tx 0, rx 1
void gpioRXTX (uint8_t val);

// set radio NSS pin to given value
void gpioNSS (uint8_t val);

bool gpioGetSleepFlag(void);

/***********************************************************************
check if button is pressed
returns true if button is pressed
***********************************************************************/
bool gpioIsButtonPressed(void);
/*******************************************************************************
  @brief
 *   Read the pad value for a button pin.
 *
 * @param None
 *
 * @return
 *   The pin value, false or true.
*******************************************************************************/
bool gpioButtonPinGet(void);
/***********************************************************************
if start button pressed returns 0x31('1')
if stop button pressed returns 0x32('2')
if nothing pressed returns -1
***********************************************************************/
int gpioGetCommand(void);

/*******************************************************************************
 if startButton(PD6) still pressed returns 0x31
  if stopButton(PD7) still pressed returns 0x32
else returns -1
*******************************************************************************/
int gpioConfCommand(void);

void gpioClearCommand(void);

void gpioClearButtonPressed(void);