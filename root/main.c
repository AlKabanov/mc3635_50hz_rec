/**************************************************************************//**
 * @file
 * @brief template for agro modem
 * @version 1.0.0
 *
 ******************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <intrinsics.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_rtc.h"

#include "gpio.h"
#include "rtc.h"
#include "spi.h"
#include "radio.h"
#include "uart.h"
#include "buffer.h"

#define POWER 10
#define MARKER 0x36
#define START 0x31
#define STOP 0x32
#define MAX_LOG (14/2)

int32_t temperature = 45;	
uint8_t fromUART;
uint8_t data[5];  
uint8_t dataLen = sizeof(data);	
receive_t RADIO_IN = {20,0,0};	//default period = 20mS, dataPtr = 0,lastSent=0,dataValid=0
uint8_t logCounter = 0;
bool transEN = false;
int command, prevCommand;
uint8_t point1 = 0, point2 = 0;
int16_t toGraph;

/**************************************************************************//**
 * @brief Update clock and wait in EM2 for RTC tick.
 *****************************************************************************/
void clockLoop(void)
{
  //radioFSKInit();
  //radioTest(data, 5, POWER);
  rtcWait(500);
  radioRXCon();
  while (1)
  {
    
    
    if(getRXDoneFlag())
    {
      radioReadRXBuf(&RADIO_IN); //check message
      if(RADIO_IN.dataValid == 1)transEN = true;
      if(bufferAdd(RADIO_IN.buffer, RADIO_IN.length, RADIO_IN.dataValid))gpioToggleLED();  //check if buffer has an empty room
      else gpioSetLED();
    }
    if(gpioIsButtonPressed()==true)  //switch radioexchange on/0ff, chanfe axe X/Y/Z
    {
      gpioClearButtonPressed();
      rtcWait(10); //10ms debounce
      if(gpioButtonPinGet() == false)
      {
        if((transEN == false))
        {
          data[0] = 1;
          radioTest(data, 5, POWER);
          bufferStop();
        }
        else
        {
          data[0] = 2;
          radioTest(data, 5, POWER);
          transEN = false;
          gpioClearLED();
        }
        rtcWait(100); 
        radioRXCon();
      }
   
    }
    if(gpioGetCommand()>0)
    {
        rtcWait(10); //10ms debounce
        command = gpioConfCommand();
        if(command == 0x31)point1 ^= 1;
        if(command == 0x32)point2 ^= 1; //toggle point2
    }
      
    if(bufferHasData())uartGraph5Send(bufferOutData(),point1,point2);
    rtcWait(100);   
    
    
    //radioRXCon();
    
    //   
  }
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
  /* Chip errata */
  CHIP_Init();

  /* Ensure core frequency has been updated */
  SystemCoreClockUpdate();
  
  gpioSetup();
  
  spiSetup();

  /* Setup RTC to generate an interrupt every minute */
  rtcSetup();
  
  uartInit(9600,true); //538uA
  
  
  gpioSetLED();
  rtcWait(200);
  gpioClearLED();
  

  /* Main function loop */
  clockLoop();

  return 0;
}
